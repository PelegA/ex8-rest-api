<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>User added</title>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
  <?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angularclass-5ef23.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

//create newUser with "push"
	$newUser=array(
		"id"=>$_POST["id"],
		"email"=>$_POST["email"],
		"name"=>$_POST["name"]
	);
	$newUserId=$firebase->push(DEFAULT_PATH.'',$newUser);
?>
<hr>
<h3 align="center"><font color="gray">User: <?echo "$newUser";?> added succesfully!</font><br></h3>
<div align="center"><a href="index.php"><button type="submit" class="btn btn-primary">Add more</button></a></div>
<hr>
</html>