<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Regular User Update</title>	  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angularclass-5ef23.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

//--get User with get
$userToUpdate1=$firebase->get(DEFAULT_PATH.'/'.(int)$_POST[id]);
$afterUpdate=$userToUpdate1;

//update existing user1 with "update"
$afterUpdate=array(
	"email"=>$_POST["email"],
	"name"=>$_POST["name"]
	);
$firebase->update(DEFAULT_PATH.'/'.(int)$_POST[id],$afterUpdate);
?>
<h3 align="center"><font color="gray"> Update succesfully!</font><br></h3>
<div align="center"><a href="index.php"><button type="submit" class="btn btn-primary">Update another user</button></a></div>


</html>