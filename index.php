<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Form - Ex8</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--<link rel="icon" type="image/x-icon" href="favicon.ico">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<?
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angularclass-5ef23.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
?>
<style>
	table, td, {
		border: 1px solid white;
		border-collapse: collapse;
	}
	th, td {
		padding: 8px;
		text-align: top;  
}
</style>
</head>
<body>
<table width="50%"> 
<tr>
	<td bgcolor="#e0e0d1">
		<h4><b>Let's Start</b></h4>
		<h5><b><font color="gray">First: Search user by ID ,then choose the action accordinglly</font></b></h5>
		<form method="post">
			<div class="form-group">
			<input type="number" class="form-control" name="id" placeholder="ID to update">
			</div>
			<button type="submit" class="btn btn-info">Search in DB</button>
			<?//--get User1 with get
				$userToUpdate=$firebase->get(DEFAULT_PATH.'/'.(int)$_POST[id]);	
				?>	
			<font color="gray">User details:</font>
			<?	
			if($userToUpdate=="null"){ 
				echo "<font color='red'>Error ! no such user in DB</font>";
			}
			else {
				echo "<font color='blue'>$userToUpdate</font>";
			}
			?>
		</form>
	</td>
</tr>
<tr>
	<td id="add">
		<hr><h4><b>Add User</b></h4>
					<form action="addUser.php" method="post">
					  <p>
						<div class="form-group">
							<label for="id">Id</label>
							<input 	type="number" 
									class="form-control"	
									name="id" 
									placeholder="numbers only">
						</div>
						  <div class="form-group">
							<label for="name">Name</label>
							<input 	type="text" 
									class="form-control"	
									name="name" 
									placeholder="first and last">
						  </div>
						  <div class="form-group">
							<label for="email">Email</label>
							<input type="text"
									class="form-control"
									name="email"
									placeholder="example@example.com">      
						  </div>
					</p>
					<button type="submit" class="btn btn-success">Add User</button>
					<button type="reset" class="btn button">Clean</button>
				</form>
	</td>
</tr>
<tr>
	<td>
	<hr><h4><b>Delete User</b></h4>
		<form action="deleteUser.php" method="post">
			
						  <p>
							<div class="form-group">
								<input 	type="number" 
										class="form-control"	
										name="id"
										placeholder="user ID to delete">
							  </div>
							</p>
						<button type="submit" class="btn btn-danger">Delete User</button>
						<button type="reset" class="btn button">Clean</button>
		</form>
	</td>
</tr>
<tr>
	<td>
		<hr><h4><b>Reguler Update</b></h4>
		<form action="updateReUser.php" method="post"> 
			<div class="form-group">
				<input type="number" class="form-control" name="id" placeholder="ID to update">
			</div>
			<div class="form-group">
				<label for="name">Name</label>
				<input 	type="text" 
						class="form-control"	
						name="name" 
						placeholder="new first and last name">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text"
						class="form-control"
						name="email"
						placeholder=" new email">      
			</div>
			<button type="submit" class="btn btn-primary" >Over/Update</button>
			<button type="reset" class="btn button">Clean</button>
		</form>
	</td>
</tr>
</table>
<br><br><br><br><br>
</body>
</html>